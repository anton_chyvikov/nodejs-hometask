const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
  // TODO: Implement methods to work with fighters
  create(data) {
    FighterRepository.create(data);
  }

  search(search) {
    const item = FighterRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }

  getAll() {
    const all = FighterRepository.getAll();
    if (!item) {
      return all;
    }
    return all;
  }
}

module.exports = new FighterService();
