const { Router } = require("express");
const FighterService = require("../services/fighterService");
const { responseMiddleware } = require("../middlewares/response.middleware");
const {
  createFighterValid,
  updateFighterValid,
} = require("../middlewares/fighter.validation.middleware");

const router = Router();

// TODO: Implement route controllers for fighter
router.post(
  "/",
  createFighterValid,
  function (req, res, next) {
    try {
      if (FighterService.search({ name: req.body.name }) === null) {
        FighterService.create(req.body);
        res.data = {
          name: req.body.name,
          power: req.body.power,
          defense: req.body.defense,
        };
      }
    } catch (err) {
      res.err = err;
    } finally {
      console.log(res.data);
      console.log(res.err);
      next();
    }
  },
  responseMiddleware
);

router.get(
  "/",
  function (req, res, next) {
    try {
      FighterService.getAll();
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

module.exports = router;
