const { Router } = require("express");
const AuthService = require("../services/authService");
const { responseMiddleware } = require("../middlewares/response.middleware");

const router = Router();

router.post(
  "/login",
  (req, res, next) => {
    try {
      // TODO: Implement login action (get the user if it exist with entered credentials)
        if (
          AuthService.login({ email: req.body.email }) ===
          AuthService.login({ password: req.body.password })
        ) {
          res.data = req.body;
        }

      // res.data = data;
    } catch (err) {
      res.err = err;
    } finally {
      console.log(res.data);
      console.log(res.err);
      next();
    }
  },
  responseMiddleware
);

module.exports = router;
