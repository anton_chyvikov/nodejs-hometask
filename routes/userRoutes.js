const { Router } = require("express");
const UserService = require("../services/userService");
const {
  createUserValid,
  updateUserValid,
} = require("../middlewares/user.validation.middleware");
const { responseMiddleware } = require("../middlewares/response.middleware");

const router = Router();

// TODO: Implement route controllers for user

router.post(
  "/", createUserValid,
  function (req, res, next) {
    try {
      if (
        UserService.search({ email: req.body.email }) === null &&
        UserService.search({ phoneNumber: req.body.phoneNumber }) === null
      ) {
        UserService.create(req.body);
        res.data = {
          email: req.body.email,
          password: req.body.password,
        };
      }
    } catch (err) {
      res.err = err;
    } finally {
      console.log(res.data);
      console.log(res.err);
      next();
    }
  },
  responseMiddleware
);

module.exports = router;
