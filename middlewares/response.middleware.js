const responseMiddleware = (req, res, next) => {
  // TODO: Implement middleware that returns result of the query
  if (res.data) {
    return res.status(200).send(res.data);
  } else if ((res.data === undefined)) {
    return res.status(404).send({
      error: true,
      message: res.err,
    });
  } else {
    return res.status(400).send({
      error: true,
      message: res.err,
    });
  }

  next();
};

exports.responseMiddleware = responseMiddleware;
