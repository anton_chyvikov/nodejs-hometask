const { fighter } = require("../models/fighter");

const createFighterValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during creation
  console.log(req.body);

  if (
    req &&
    req.body &&
    !req.body.id &&
    req.body.name &&
    req.body.power &&
    isPowerAvailable(req.body.power) &&
    req.body.defense &&
    isDefenceAvailable(req.body.defense) &&
    hasForbiddenKeys(req.body)
  ) {
    if (req.body.health) {
      if (isHealthAvailable(req.body.health)) {
        next();
      } else {
        res.status(400).send();
      }
    } else {
      next();
    }
  } else {
    res.status(400).send();
  }
};

const updateFighterValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during update
  next();
};

const isPowerAvailable = (power) => {
  return !isNaN(power) && power > 1 && power < 100;
};

const isDefenceAvailable = (defense) => {
  return !isNaN(defense) && defense > 1 && defense < 10;
};

const isHealthAvailable = (health) => {
  return !isNaN(health) && health > 80 && health < 120;
};

const hasForbiddenKeys = (body) => {
  return Object.keys(body).length < 5;
};

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
