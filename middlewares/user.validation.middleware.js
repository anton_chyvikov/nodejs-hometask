const { user } = require("../models/user");
const createUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during creation

  if (
    req &&
    req.body &&
    !req.body.id &&
    req.body.firstName &&
    req.body.lastName &&
    req.body.phoneNumber &&
    isPhoneNumberValid(req.body.phoneNumber) &&
    req.body.email &&
    isGmail(req.body.email) &&
    req.body.password &&
    isPasswordAvailable(req.body.password) &&
    !hasForbiddenKeys(req.body)
  ) {
    next();
  } else {
    res.status(400).send();
  }
};

const updateUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during update

  next();
};

const isGmail = (mail) => {
  const emailCheck =
    /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*(@gmail?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
  return emailCheck.test(mail);
};

const isPhoneNumberValid = (phoneNumber) => {
  const phoneCheck = /^(\+380)\d{9}$/;
  return phoneCheck.test(phoneNumber);
};

const isPasswordAvailable = (password) => {
  return password.length > 2;
};

const hasForbiddenKeys = (body) => {
  return Object.keys(body).length !== 5;
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
